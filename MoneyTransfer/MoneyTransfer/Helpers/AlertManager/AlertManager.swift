//
//  AlertManager.swift
//  MoneyTransfer
//
//  Created by Binoj V Janardhanan on 15/2/2019.
//  Copyright © 2019 MyBank. All rights reserved.
//

import UIKit

class AlertManager {
    class func showAlert(error: MTError, action: (()-> Void)? = nil, cancelAction: (()-> Void)? = nil, inView view: UIView) {
        let alert = AlertView()
        alert.configAlert(error: error, action: action, cancelAction: cancelAction)
        alert.show(inView: view)
    }
    
    class func showAlert(title: String, message: String, actionTitle: String? = nil, cancelTitle: String? = nil, action: (()-> Void)? = nil, cancelAction: (()-> Void)? = nil, inView view: UIView) {
        let alert = AlertView()
        alert.configAlert(title: title, message: message, actionTitle: actionTitle ?? "", cancelTitle: cancelTitle ?? "OK", action: action, cancelAction: cancelAction)
        alert.show(inView: view)
    }
}
