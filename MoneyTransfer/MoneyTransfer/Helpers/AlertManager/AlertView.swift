//
//  AlertView.swift
//  MoneyTransfer
//
//  Created by Binoj V Janardhanan on 15/2/2019.
//  Copyright © 2019 MyBank. All rights reserved.
//

import UIKit
import UIDeviceComplete
import PureLayout

class AlertView: UIView {
    
    //=============================================================
    // MARK:- Constants
    //=============================================================
    
    struct Constants {
        static let ContentMargin: CGFloat = 20.0
        static let AlertMinHeight: CGFloat = 200.0
        static let AlertButtonHeight: CGFloat = 40.0
        
        static let iPadWidthMultiplier: CGFloat = 0.5
        static let iPhoneWidthMultiplier: CGFloat = 0.9
    }
    
    //=============================================================
    // MARK:- UI Items
    //=============================================================
    
    private var titleLabel: UILabel = UILabel()
    private var messageLabel: UILabel = UILabel()
    
    private var actionButton: UIButton = UIButton(type: UIButton.ButtonType.custom)
    private var cancelButton: UIButton = UIButton(type: UIButton.ButtonType.custom)
    
    private var alertView: UIView = UIView()
    
    //=============================================================
    // MARK:- Variables
    //=============================================================
    
    private var alertAction: (() -> Void)?
    private var cancelAction: (() -> Void)?
    
    //=============================================================
    // MARK:- Initial Setup methods
    //=============================================================
    
    private func setupView() {
        self.addAlertView()
        
        ThemeManager.sharedInstance.alertViewContentViewStyle(view: self)
        ThemeManager.sharedInstance.alertViewAlertViewStyle(view: self.alertView)
        ThemeManager.sharedInstance.alertViewTitleLabelStyle(label: self.titleLabel)
        ThemeManager.sharedInstance.alertViewMessgeLabelStyle(label: self.messageLabel)
        ThemeManager.sharedInstance.alertViewActionButtonStyle(button: self.actionButton)
        ThemeManager.sharedInstance.alertViewCancelButtonStyle(button: self.cancelButton)
        self.alpha = 0.0
    }
    
    private func setupControls() {
        self.actionButton.addTarget(self, action: #selector(self.actionButtonTapped), for: UIControl.Event.touchUpInside)
        self.cancelButton.addTarget(self, action: #selector(self.cancelButtonTapped), for: UIControl.Event.touchUpInside)
    }
    
    private func addAlertView() {
        self.addSubview(self.alertView)
        
        if UIDevice.current.dc.isIpad {
            self.alertView.autoMatch(.width, to: .width, of: self, withMultiplier: Constants.iPadWidthMultiplier)
        }
        else {
            self.alertView.autoMatch(.width, to: .width, of: self, withMultiplier: Constants.iPhoneWidthMultiplier)
        }
        
        self.alertView.autoSetDimension(.height, toSize: Constants.AlertMinHeight, relation: .greaterThanOrEqual)
        self.alertView.autoCenterInSuperview()
        
        self.alertView.addSubview(self.titleLabel)
        self.titleLabel.autoPinEdge(toSuperviewEdge: .leading, withInset: Constants.ContentMargin)
        self.titleLabel.autoPinEdge(toSuperviewEdge: .trailing, withInset: Constants.ContentMargin)
        self.titleLabel.autoPinEdge(toSuperviewEdge: .top, withInset: Constants.ContentMargin)
        
        self.alertView.addSubview(self.messageLabel)
        self.messageLabel.autoPinEdge(.leading, to: .leading, of: self.titleLabel)
        self.messageLabel.autoPinEdge(.trailing, to: .trailing, of: self.titleLabel)
        self.messageLabel.autoPinEdge(.top, to: .bottom, of: self.titleLabel, withOffset: Constants.ContentMargin)
        
        self.alertView.addSubview(self.actionButton)
        self.alertView.addSubview(self.cancelButton)
        
        self.actionButton.autoPinEdge(.leading, to: .leading, of: self.messageLabel)
        self.actionButton.autoPinEdge(.top, to: .bottom, of: self.messageLabel, withOffset: Constants.ContentMargin, relation: .greaterThanOrEqual)
        self.actionButton.autoSetDimension(.height, toSize: Constants.AlertButtonHeight)
        
        self.cancelButton.autoPinEdge(.trailing, to: .trailing, of: self.messageLabel)
        self.cancelButton.autoPinEdge(.top, to: .top, of: self.actionButton)
        self.cancelButton.autoMatch(.height, to: .height, of: self.actionButton)
        self.cancelButton.autoMatch(.width, to: .width, of: self.actionButton)
        self.cancelButton.autoPinEdge(.leading, to: .trailing, of: self.actionButton, withOffset: Constants.ContentMargin)
        
        self.actionButton.autoPinEdge(toSuperviewEdge: .bottom, withInset: Constants.ContentMargin)
    }
    
    //=============================================================
    // MARK:- Public methods
    //=============================================================
    
    func configAlert(error: MTError, action: (()-> Void)? = nil, cancelAction: (()-> Void)? = nil) {
        self.setupView()
        self.setupControls()
        
        self.alertAction = action
        self.actionButton.isHidden = (action == nil)
        self.cancelAction = cancelAction
        self.titleLabel.text = error.errorTitle
        self.messageLabel.text = error.errorMessage
        self.actionButton.setTitle(error.actionTitle, for: .normal)
        self.cancelButton.setTitle(error.cancelTitle, for: .normal)
    }
    
    func configAlert(title: String, message: String, actionTitle: String, cancelTitle: String, action: (()-> Void)? = nil, cancelAction: (()-> Void)? = nil) {
        self.setupView()
        self.setupControls()
        
        self.alertAction = action
        self.actionButton.isHidden = (action == nil)
        self.cancelAction = cancelAction
        self.titleLabel.text = title
        self.messageLabel.text = message
        self.actionButton.setTitle(actionTitle, for: .normal)
        self.cancelButton.setTitle(cancelTitle, for: .normal)
    }
    
    //=============================================================
    // MARK:- Action methods
    //=============================================================
    
    @objc func actionButtonTapped() {
        self.alertAction?()
        self.alertAction = nil
        self.hide()
    }
    
    @objc func cancelButtonTapped() {
        self.cancelAction?()
        self.cancelAction = nil
        self.hide()
    }
    
    //=============================================================
    // MARK:- Helper methods
    //=============================================================
    
    func show(inView view: UIView) {
        view.addSubview(self)
        self.autoPinEdgesToSuperviewEdges()
        ThemeManager.sharedInstance.alertViewActionButtonStyle(button: self.actionButton)
        ThemeManager.sharedInstance.alertViewCancelButtonStyle(button: self.cancelButton)
        UIView.animate(withDuration: 0.3) {
            self.alpha = 1.0
        }
    }
    
    fileprivate func hide() {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.alpha = 0.0
        }, completion: { (finished: Bool) -> Void in
            self.removeFromSuperview()
        })
    }
}


