//
//  MTError.swift
//  MoneyTransfer
//
//  Created by Binoj V Janardhanan on 15/2/2019.
//  Copyright © 2019 MyBank. All rights reserved.
//

import Foundation

class MTError: NSError {
    
    //===========================================================
    //MARK:- Variables
    //===========================================================
    
    var errorTitle: String = ""
    var errorMessage: String = ""
    var actionTitle: String = ""
    var cancelTitle: String = ""
    
    //===========================================================
    //MARK:- Init Methods
    //===========================================================
    
    init(error: NSError, errorTitle: String, errorMessage: String, actionTitle: String, cancelTitle: String) {
        super.init(domain: error.domain, code: error.code, userInfo: nil)
        self.errorTitle = errorTitle
        self.errorMessage = errorMessage
        self.actionTitle = actionTitle
        self.cancelTitle = cancelTitle
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


