//
//  ErrorManager.swift
//  MoneyTransfer
//
//  Created by Binoj V Janardhanan on 15/2/2019.
//  Copyright © 2019 MyBank. All rights reserved.
//

import Foundation

class ErrorManager {
    
    //=============================================================
    // MARK:- Constants
    //=============================================================
    
    struct Constants {
        static let Domain: String = "com.moneytransfer.error"
    }
    
}

//=============================================================
// MARK:- Errors for common api error codes
//=============================================================

extension ErrorManager {
    enum ApiErrorResponseCodes : String, Error {
        case NetworkError        = "-1009"
        case InternalServerError = "500"
        case NotImplemented      = "501"
        case BadGateway          = "502"
        case ServiceUnavailable  = "503"
        case NotFound            = "404"
        case UnknownError        = "422"
        
        var errorCode: String {
            return self.rawValue
        }
        
        var errorCodeNumber : Int {
            return Int(self.rawValue)!
        }
    }
    
    class func ApiErrorFor(errorCode : Int) -> MTError {
        
        let error = NSError(domain: Constants.Domain, code: errorCode, userInfo: nil)
        
        switch errorCode {
            
        case ApiErrorResponseCodes.NetworkError.errorCodeNumber :
            return MTError.init(error: error, errorTitle: "You are not connected!", errorMessage: "Please check your internet connection and try again.", actionTitle: "Try Again", cancelTitle: "OK")
            
        case ApiErrorResponseCodes.InternalServerError.errorCodeNumber...ApiErrorResponseCodes.ServiceUnavailable.errorCodeNumber :
            return MTError.init(error: error, errorTitle: "Server error!", errorMessage: "Couldn't get connected to the server. Please try again later.", actionTitle: "Try Again", cancelTitle: "OK")
            
        case ApiErrorResponseCodes.UnknownError.errorCodeNumber :
            fallthrough
            
        default:
            return MTError.init(error: error, errorTitle: "Something went wrong!", errorMessage: "Please try again later.", actionTitle: "Try Again", cancelTitle: "OK")
        }
        
    }
}



