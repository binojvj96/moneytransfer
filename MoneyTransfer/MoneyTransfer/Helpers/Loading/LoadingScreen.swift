//
//  LoadingScreen.swift
//  MoneyTransfer
//
//  Created by Binoj V Janardhanan on 18/2/2019.
//  Copyright © 2019 MyBank. All rights reserved.
//

import UIKit

class LoadingScreen: UIView {
    
    //==========================================================================
    //MARK:- UI Items
    //==========================================================================
    
    fileprivate var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    //==========================================================================
    //MARK:- Public methods
    //==========================================================================
    
    func show(inView view: UIView) {
        self.setupView()
        view.addSubview(self)
        self.autoPinEdgesToSuperviewEdges()
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 1.0
        }) { (done) in
            self.activityIndicator.startAnimating()
        }
    }
    
    func hide() {
        self.activityIndicator.stopAnimating()
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0.0
        }) { (done) in
            self.removeFromSuperview()
        }
    }
    
    //==========================================================================
    //MARK:- Private setup methods
    //==========================================================================
    
    private func setupView() {
        self.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.addSubview(self.activityIndicator)
        self.activityIndicator.autoCenterInSuperviewMargins()
        self.alpha = 0
    }
    
}

