//
//  ThemeManager.swift
//  MoneyTransfer
//
//  Created by Binoj V Janardhanan on 14/2/2019.
//  Copyright © 2019 MyBank. All rights reserved.
//

import UIKit
import Foundation
import UIDeviceComplete

class ThemeManager {
    //==========================================================================
    //MARK:- Variables
    //==========================================================================
    
    public static let sharedInstance = ThemeManager()
    
    private var theme: Theme
    
    //==========================================================================
    //MARK:- Init Methods
    //==========================================================================
    init() {
        if UIDevice.current.dc.isIpad {
            self.theme = iPadTheme()
        }
        else {
            self.theme = iPhoneTheme()
        }
    }
}

extension ThemeManager: Theme {
    
    //==========================================================================
    //MARK:- Home View Controller
    //==========================================================================
    
    func homeVCViewStyle(view: UIView) {
        self.theme.homeVCViewStyle(view: view)
    }
    
    func homeVCMainContainerViewStyle(view: UIView) {
        self.theme.homeVCMainContainerViewStyle(view: view)
    }
    
    func homeVCAccountBalanceCardViewStyle(view: UIView) {
        self.theme.homeVCAccountBalanceCardViewStyle(view: view)
    }
    
    //==========================================================================
    //MARK:- Account Overview Card
    //==========================================================================
    
    func accountOverviewCardBalanceTitleLabelStyle(label: UILabel) {
        self.theme.accountOverviewCardBalanceTitleLabelStyle(label: label)
    }
    
    func accountOverviewCardBalanceLabelStyle(label: UILabel) {
        self.theme.accountOverviewCardBalanceLabelStyle(label: label)
    }
    
    func accountOverviewCardBalanceSeparatorViewStyle(view: UIView) {
        self.theme.accountOverviewCardBalanceSeparatorViewStyle(view: view)
    }
    
    //==========================================================================
    //MARK:- Transfer View Controller
    //==========================================================================
    
    func transferVCViewStyle(view : UIView) {
        self.theme.transferVCViewStyle(view: view)
    }
    
    func transferVCMainContainerViewStyle(view: UIView) {
        self.theme.transferVCMainContainerViewStyle(view: view)
    }
    
    func transferVCAccountCardViewStyle(view: UIView) {
        self.theme.transferVCAccountCardViewStyle(view: view)
    }
    
    func transferVCMakeTransferButtonStyle(button: UIButton) {
        self.theme.transferVCMakeTransferButtonStyle(button: button)
    }
    
    func transferVCAccountBalanceLabelStyle(label: UILabel) {
        self.theme.transferVCAccountBalanceLabelStyle(label: label)
    }
    
    func transferVCAccountTransferTextFieldStyle(textField: UITextField) {
        self.theme.transferVCAccountTransferTextFieldStyle(textField: textField)
    }
    
    func transferVCBalanceSeparatorViewStyle(view: UIView) {
        self.theme.transferVCBalanceSeparatorViewStyle(view: view)
    }
    
    //==========================================================================
    //MARK:- Alert View
    //==========================================================================
    
    func alertViewContentViewStyle(view: UIView) {
        self.theme.alertViewContentViewStyle(view: view)
    }
    
    func alertViewAlertViewStyle(view: UIView) {
        self.theme.alertViewAlertViewStyle(view: view)
    }
    
    func alertViewTitleLabelStyle(label: UILabel) {
        self.theme.alertViewTitleLabelStyle(label: label)
    }
    
    func alertViewMessgeLabelStyle(label: UILabel) {
        self.theme.alertViewMessgeLabelStyle(label: label)
    }
    
    func alertViewActionButtonStyle(button: UIButton) {
        self.theme.alertViewActionButtonStyle(button: button)
    }
    
    func alertViewCancelButtonStyle(button: UIButton) {
        self.theme.alertViewCancelButtonStyle(button: button)
    }
}
