//
//  Theme.swift
//  MoneyTransfer
//
//  Created by Binoj V Janardhanan on 14/2/2019.
//  Copyright © 2019 MyBank. All rights reserved.
//

import UIKit

protocol Theme {
    
    //==========================================================================
    //MARK:- Home View Controller
    //==========================================================================
    
    func homeVCViewStyle(view : UIView)
    func homeVCMainContainerViewStyle(view: UIView)
    func homeVCAccountBalanceCardViewStyle(view: UIView)
    
    //==========================================================================
    //MARK:- Account Overview Card
    //==========================================================================
    
    func accountOverviewCardBalanceTitleLabelStyle(label: UILabel)
    func accountOverviewCardBalanceLabelStyle(label: UILabel)
    func accountOverviewCardBalanceSeparatorViewStyle(view: UIView)
    
    //==========================================================================
    //MARK:- Transfer View Controller
    //==========================================================================
    
    func transferVCViewStyle(view : UIView)
    func transferVCMainContainerViewStyle(view: UIView)
    func transferVCAccountCardViewStyle(view: UIView)
    func transferVCMakeTransferButtonStyle(button: UIButton)
    func transferVCAccountBalanceLabelStyle(label: UILabel)
    func transferVCAccountTransferTextFieldStyle(textField: UITextField)
    func transferVCBalanceSeparatorViewStyle(view: UIView)
    
    //==========================================================================
    //MARK:- Alert View
    //==========================================================================
    
    func alertViewContentViewStyle(view: UIView)
    func alertViewAlertViewStyle(view: UIView)
    func alertViewTitleLabelStyle(label: UILabel)
    func alertViewMessgeLabelStyle(label: UILabel)
    func alertViewActionButtonStyle(button: UIButton)
    func alertViewCancelButtonStyle(button: UIButton)
}
