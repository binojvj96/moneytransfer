//
//  iPhoneTheme.swift
//  MoneyTransfer
//
//  Created by Binoj V Janardhanan on 14/2/2019.
//  Copyright © 2019 MyBank. All rights reserved.
//

import UIKit
import UIDeviceComplete

class iPhoneTheme: Theme {
    
    //==========================================================================
    //MARK:- Home View Controller
    //==========================================================================
    
    func homeVCViewStyle(view : UIView) {
        view.backgroundColor = UIColor.white
    }
    
    func homeVCMainContainerViewStyle(view: UIView) {
        view.backgroundColor = UIColor.white
    }
    
    func homeVCAccountBalanceCardViewStyle(view: UIView) {
        view.backgroundColor = UIColor.white
        
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.borderWidth = 0.5
        view.layer.cornerRadius = 10.0
        view.clipsToBounds = true
        
        view.layer.shadowPath =
            UIBezierPath(roundedRect: view.bounds,
                         cornerRadius: view.layer.cornerRadius).cgPath
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: -3, height: 3)
        view.layer.shadowRadius = 1
        view.layer.masksToBounds = false
    }
    
    //==========================================================================
    //MARK:- Account Overview Card
    //==========================================================================
    
    func accountOverviewCardBalanceTitleLabelStyle(label: UILabel) {
        label.text = "Account Balance"
        label.font = UIFont.systemFont(ofSize: 18.0)
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        label.numberOfLines = 1
    }
    
    func accountOverviewCardBalanceLabelStyle(label: UILabel) {
        label.text = "0.00 HKD"
        label.textColor = UIColor.blue
        label.font = UIFont.boldSystemFont(ofSize: 20.0)
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        label.numberOfLines = 1
    }
    
    func accountOverviewCardBalanceSeparatorViewStyle(view: UIView) {
        view.backgroundColor = UIColor.gray
        view.layer.cornerRadius = 1.0
        view.layer.masksToBounds = true
    }
    
    //==========================================================================
    //MARK:- Transfer View Controller
    //==========================================================================
    
    func transferVCViewStyle(view : UIView) {
        self.homeVCViewStyle(view: view)
    }
    
    func transferVCMainContainerViewStyle(view: UIView) {
        self.homeVCMainContainerViewStyle(view: view)
    }
    
    func transferVCAccountCardViewStyle(view: UIView) {
        view.backgroundColor = UIColor.white
        
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.borderWidth = 0.5
        view.layer.cornerRadius = 10.0
        view.clipsToBounds = true
    }
    
    func transferVCMakeTransferButtonStyle(button: UIButton) {
        button.setTitle("Make Transfer", for: .normal)
        
        button.backgroundColor = UIColor.white
        
        button.setTitleColor(UIColor.black, for: .normal)
        button.setTitleColor(UIColor.lightGray, for: .highlighted)
        button.setTitleColor(UIColor.lightGray, for: .selected)
        
        button.layer.borderColor = UIColor.lightGray.cgColor
        button.layer.borderWidth = 1.0
        button.layer.cornerRadius = button.frame.size.height/2
        button.layer.masksToBounds = true
        button.clipsToBounds = true
        
        button.layer.shadowPath =
            UIBezierPath(roundedRect: button.bounds,
                         cornerRadius: button.layer.cornerRadius).cgPath
        button.layer.shadowColor = UIColor.gray.cgColor
        button.layer.shadowOpacity = 0.5
        button.layer.shadowOffset = CGSize(width: -3, height: 3)
        button.layer.shadowRadius = 1
        button.layer.masksToBounds = false
    }
    
    func transferVCAccountBalanceLabelStyle(label: UILabel) {
        self.accountOverviewCardBalanceLabelStyle(label: label)
    }
    
    func transferVCAccountTransferTextFieldStyle(textField: UITextField) {
        textField.borderStyle = .roundedRect
        textField.font = UIFont.boldSystemFont(ofSize: 18.0)
        textField.placeholder = "Transfer Amount"
        textField.textAlignment = .center
        textField.keyboardType = .numberPad
        textField.tintColor = UIColor.black
        textField.textColor = UIColor.black
    }
    
    func transferVCBalanceSeparatorViewStyle(view: UIView) {
        self.accountOverviewCardBalanceSeparatorViewStyle(view: view)
    }
    
    //==========================================================================
    //MARK:- Alert View
    //==========================================================================
    
    func alertViewContentViewStyle(view: UIView) {
        view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
    }
    
    func alertViewAlertViewStyle(view: UIView) {
        view.backgroundColor = UIColor.darkGray
        view.layer.cornerRadius = 10.0
        view.layer.masksToBounds = true
    }
    
    func alertViewTitleLabelStyle(label: UILabel) {
        let fontSize: CGFloat = ((UIDevice.current.dc.screenSize.sizeInches ?? 4.0) < 5.0) ? 16.0 : 18.0
        label.font = UIFont.systemFont(ofSize: fontSize)
        label.textColor = UIColor.white
        label.numberOfLines = 2
        label.lineBreakMode = .byWordWrapping
    }
    
    func alertViewMessgeLabelStyle(label: UILabel) {
        let fontSize: CGFloat = ((UIDevice.current.dc.screenSize.sizeInches ?? 4.0) < 5.0) ? 11.0 : 14.0
        label.font = UIFont.systemFont(ofSize: fontSize)
        label.textColor = UIColor.white.withAlphaComponent(0.8)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
    }
    
    func alertViewActionButtonStyle(button: UIButton) {
        button.setTitle("Try Again", for: .normal)
        
        let fontSize: CGFloat = ((UIDevice.current.dc.screenSize.sizeInches ?? 4.0) < 5.0) ? 13.0 : 16.0
        button.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        button.titleLabel?.textColor = UIColor.white
        button.backgroundColor = UIColor.purple
        button.layer.cornerRadius = AlertView.Constants.AlertButtonHeight/2
        button.layer.masksToBounds = true
    }
    
    func alertViewCancelButtonStyle(button: UIButton) {
        button.setTitle("OK", for: .normal)
        let fontSize: CGFloat = ((UIDevice.current.dc.screenSize.sizeInches ?? 4.0) < 5.0) ? 13.0 : 16.0
        button.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        button.titleLabel?.textColor = UIColor.white
        button.backgroundColor = UIColor.clear
        button.layer.cornerRadius = AlertView.Constants.AlertButtonHeight/2
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.borderWidth = 1.0
        button.layer.masksToBounds = true
    }
}
