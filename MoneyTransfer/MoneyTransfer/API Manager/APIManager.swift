//
//  APIManager.swift
//  MoneyTransfer
//
//  Created by Binoj V Janardhanan on 18/2/2019.
//  Copyright © 2019 MyBank. All rights reserved.
//

import Alamofire
import PromiseKit
import ObjectMapper

class ApiManager {
    
    //==========================================================================
    //MARK:- Constants
    //==========================================================================
    
    struct Constants{
        static let Domain: String = "http://localhost:5000/api/v1"
        struct EndPoints {
            static let GetAccount: String = "/accounts/" //GET
            static let MakeTransfer: String = "/maketransfer" //PUT
        }
        
        struct ParamKeys {
            static let FromAccountID: String = "fromAccountId"
            static let ToAccountID: String = "toAccountId"
            static let TransferAmount: String = "transferAmount"
        }
        
        struct StatusCodes {
            static let GETSuccessCode: Int = 200
            static let PUTSuccessCode: Int = 201
        }
    }
    
    //==========================================================================
    //MARK:- Variables
    //==========================================================================
    
    static let sharedInstance: ApiManager = ApiManager()
    
    //==========================================================================
    //MARK:- Api methods
    //==========================================================================
    
    func getAccount(accountId: Int) -> Promise<Account> {
        return Promise(resolver: { (resolve) in
            var urlString = Constants.Domain + Constants.EndPoints.GetAccount
            urlString.append(String(accountId))
            if let url = URL(string: urlString) {
                Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: { (responseObject) in
                    
                    let data = responseObject.data
                    let error = responseObject.error
                    let statusCode = responseObject.response?.statusCode
                    
                    if let error = error as NSError? {
                        resolve.reject(ErrorManager.ApiErrorFor(errorCode: statusCode ?? error.code))
                    }
                    else if let data = data, statusCode == Constants.StatusCodes.GETSuccessCode {
                        if let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments), let account = Mapper<Account>().map(JSONObject: json) {
                            resolve.fulfill(account)
                        }
                        else {
                            resolve.reject(ErrorManager.ApiErrorFor(errorCode: 0))
                        }
                    }
                    else {
                        resolve.reject(ErrorManager.ApiErrorFor(errorCode: statusCode ?? 0))
                    }
                })
            }
            else {
                resolve.reject(ErrorManager.ApiErrorFor(errorCode: 0))
            }
        })
    }
    
    func makeTransfer(fromAccount: Account, toAccount: Account, amount: Double) -> Promise<Account> {
        return Promise(resolver: { (resolve) in
            let urlString = Constants.Domain + Constants.EndPoints.MakeTransfer
            var params: [String: Any] = [:]
            params[Constants.ParamKeys.FromAccountID] = fromAccount.accountId
            params[Constants.ParamKeys.ToAccountID] = toAccount.accountId
            params[Constants.ParamKeys.TransferAmount] = amount
            
            if let url = URL(string: urlString) {
                Alamofire.request(url, method: .put, parameters: params, encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: { (responseObject) in
                    
                    let data = responseObject.data
                    let error = responseObject.error
                    let statusCode = responseObject.response?.statusCode
                    
                    if let error = error as NSError? {
                        resolve.reject(ErrorManager.ApiErrorFor(errorCode: statusCode ?? error.code))
                    }
                    else if let data = data, statusCode == Constants.StatusCodes.PUTSuccessCode {
                        if let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments), let account = Mapper<Account>().map(JSONObject: json) {
                            resolve.fulfill(account)
                        }
                        else {
                            resolve.reject(ErrorManager.ApiErrorFor(errorCode: 0))
                        }
                    }
                    else {
                        resolve.reject(ErrorManager.ApiErrorFor(errorCode: statusCode ?? 0))
                    }
                })
            }
            else {
                resolve.reject(ErrorManager.ApiErrorFor(errorCode: 0))
            }
        })
    }
    
}
