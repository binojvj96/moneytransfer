//
//  TransferViewWorker.swift
//  MoneyTransfer
//
//  Created by Binoj V Janardhanan on 19/2/2019.
//  Copyright © 2019 MyBank. All rights reserved.
//

import PromiseKit

protocol TransferViewWorkerProtocol {
    func transferCompleted(account: Account)
    func showError(error: MTError)
}

class TransferViewWorker {
    //==========================================================================
    //MARK:- Constants
    //==========================================================================
    
    private struct Constants {
        static let MinimumTransferAmount: Double = 100
    }
    
    //==========================================================================
    //MARK:- Variables
    //==========================================================================
    
    var delegate: TransferViewWorkerProtocol?
    
    //==========================================================================
    //MARK:- Public Methods
    //==========================================================================
    
    func makeTransfer(fromAccount: Account, toAccount: Account, amount: Double) {
        if !self.checkMinimumTransferAMount(amount: amount) {
            self.sendError(message: "Minimum transfer amount is \(Constants.MinimumTransferAmount).")
            return
        }
        
        if self.checkBalance(account: fromAccount, amount: amount) {
            firstly { () -> Promise<Account> in
                return ApiManager.sharedInstance.makeTransfer(fromAccount: fromAccount, toAccount: toAccount, amount: amount)
                }.done { (account) in
                    self.delegate?.transferCompleted(account: account)
                }.catch { (error) in
                    if let error = error as? MTError {
                        self.delegate?.showError(error: error)
                    }
                    else {
                        self.sendError(message: "Something Went Wrong!")
                    }
            }
        }
        else {
            self.sendError(message: "Not enough balance to do the transfer.")
        }
    }
    
    //==========================================================================
    //MARK:- Helper Methods
    //==========================================================================
    
    private func checkBalance(account: Account, amount: Double) -> Bool {
        return amount <= account.balance
    }
    
    private func checkMinimumTransferAMount(amount: Double) -> Bool {
        return amount >= Constants.MinimumTransferAmount
    }
    
    private func sendError(message: String) {
        let error: MTError = MTError(error: NSError(), errorTitle: "Error!", errorMessage: message, actionTitle: "", cancelTitle: "OK")
        self.delegate?.showError(error: error)
    }
}
