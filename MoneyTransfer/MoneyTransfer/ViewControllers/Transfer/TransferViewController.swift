//
//  TransferViewController.swift
//  MoneyTransfer
//
//  Created by Binoj V Janardhanan on 19/2/2019.
//  Copyright © 2019 MyBank. All rights reserved.
//

import UIKit

class TransferViewController: UIViewController {
    
    //==========================================================================
    //MARK:- UI Variables
    //==========================================================================
    
    private var mainContainerView: UIView = UIView()
    private var accountTransferCardContainerView: UIView = UIView()
    private var accountBalanceSeparatorView: UIView = UIView()
    private var transferTextFieldContainerView: UIView = UIView()
    
    private var makeTransferButton: UIButton!
    
    private var accountBalanceLabel: UILabel = UILabel()
    
    private var transferAmountTextField: UITextField = UITextField()
    
    private var loadingScreen: LoadingScreen = LoadingScreen()
    
    //==========================================================================
    //MARK:- Variables
    //==========================================================================
    
    private var fromAccount: Account?
    private var toAccount: Account?
    
    private var transferViewWorker: TransferViewWorker = TransferViewWorker()
    
    //==========================================================================
    //MARK:- Constants
    //==========================================================================
    
    public struct Constants {
        static let CommonMarginFromEdges: CGFloat = HomeViewController.Constants.CommonMarginFromEdges
        static let MainContainerLeftRightMargin: CGFloat = HomeViewController.Constants.MainContainerLeftRightMargin
        static let CardContainerHeight: CGFloat = 150.0
        static let TransferTextfieldHeight: CGFloat = 50.0
        static let TransferButtonHeight: CGFloat = 50.0
        static let SeparatorViewHeight: CGFloat = 2.0
        static let TextFieldCharacterLimit: Int = 10
    }
    
    //==========================================================================
    //MARK:- Lifecycle Methods
    //==========================================================================
    
    convenience init(fromAccount: Account, toAccount: Account) {
        self.init()
        
        self.fromAccount = fromAccount
        self.toAccount = toAccount
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white //Weird bug that causes lag when pushing if not added this code.
        
        self.setupController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.applyViewStyle()
        self.finishSetup()
        self.view.setNeedsLayout()
    }
    
    //==========================================================================
    //MARK:- Initial Setup methods
    //==========================================================================
    
    private func setupController() {
        self.transferViewWorker.delegate = self
        self.makeTransferButton = UIButton(type: .roundedRect)
        self.makeTransferButton.addTarget(self, action: #selector(makeTransferButtonPressed), for: .touchUpInside)
        self.transferAmountTextField.delegate = self
    }
    
    private func setupView() {
        
        self.view.addSubview(self.mainContainerView)
        self.mainContainerView.autoPinEdgesToSuperviewSafeArea()
        
        self.mainContainerView.addSubview(self.accountTransferCardContainerView)
        self.accountTransferCardContainerView.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: Constants.CommonMarginFromEdges, left: Constants.MainContainerLeftRightMargin, bottom: 0.0, right: Constants.MainContainerLeftRightMargin), excludingEdge: .bottom)
        self.accountTransferCardContainerView.autoSetDimension(.height, toSize: Constants.CardContainerHeight)
        
        self.setupCardView()
        
        self.mainContainerView.addSubview(self.makeTransferButton)
        self.makeTransferButton.autoPinEdge(.leading, to: .leading, of: self.accountTransferCardContainerView)
        self.makeTransferButton.autoPinEdge(.trailing, to: .trailing, of: self.accountTransferCardContainerView)
        self.makeTransferButton.autoPinEdge(.top, to: .bottom, of: self.accountTransferCardContainerView, withOffset: Constants.CommonMarginFromEdges)
        self.makeTransferButton.autoSetDimension(.height, toSize: Constants.TransferButtonHeight)
    }
    
    private func setupCardView() {
        self.accountTransferCardContainerView.addSubview(self.accountBalanceLabel)
        self.accountBalanceLabel.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: Constants.CommonMarginFromEdges, left: Constants.CommonMarginFromEdges, bottom: 0.0, right: Constants.CommonMarginFromEdges), excludingEdge: .bottom)
        
        self.accountTransferCardContainerView.addSubview(self.accountBalanceSeparatorView)
        self.accountBalanceSeparatorView.autoPinEdge(toSuperviewEdge: .leading, withInset: Constants.CommonMarginFromEdges)
        self.accountBalanceSeparatorView.autoPinEdge(toSuperviewEdge: .trailing, withInset: Constants.CommonMarginFromEdges)
        self.accountBalanceSeparatorView.autoPinEdge(.top, to: .bottom, of: self.accountBalanceLabel, withOffset:Constants.CommonMarginFromEdges)
        self.accountBalanceSeparatorView.autoSetDimension(.height, toSize: Constants.SeparatorViewHeight)
        
        self.accountTransferCardContainerView.addSubview(self.transferTextFieldContainerView)
        self.transferTextFieldContainerView.backgroundColor = UIColor.clear
        self.transferTextFieldContainerView.autoPinEdges(toSuperviewMarginsExcludingEdge: .top)
        self.transferTextFieldContainerView.autoPinEdge(.top, to: .bottom, of: self.accountBalanceSeparatorView)
        
        self.transferTextFieldContainerView.addSubview(self.transferAmountTextField)
        self.transferAmountTextField.autoCenterInSuperview()
        self.transferAmountTextField.autoPinEdge(toSuperviewEdge: .leading, withInset: Constants.CommonMarginFromEdges)
        self.transferAmountTextField.autoPinEdge(toSuperviewEdge: .trailing, withInset: Constants.CommonMarginFromEdges)
        self.transferAmountTextField.autoSetDimension(.height, toSize: Constants.TransferTextfieldHeight)
    }
    
    private func applyViewStyle() {
        ThemeManager.sharedInstance.transferVCViewStyle(view: self.view)
        ThemeManager.sharedInstance.transferVCMainContainerViewStyle(view: self.mainContainerView)
        ThemeManager.sharedInstance.transferVCAccountCardViewStyle(view: self.accountTransferCardContainerView)
        ThemeManager.sharedInstance.transferVCAccountBalanceLabelStyle(label: self.accountBalanceLabel)
        ThemeManager.sharedInstance.transferVCBalanceSeparatorViewStyle(view: self.accountBalanceSeparatorView)
        ThemeManager.sharedInstance.transferVCAccountTransferTextFieldStyle(textField: self.transferAmountTextField)
        ThemeManager.sharedInstance.transferVCMakeTransferButtonStyle(button: self.makeTransferButton)
    }
    
    private func finishSetup() {
        self.accountBalanceLabel.text = "Account \(self.fromAccount?.accountId ?? 0) Balance : \(self.fromAccount?.balance ?? 0.00)"
        self.transferAmountTextField.text = ""
    }
    
    //==========================================================================
    //MARK:- Action Methods
    //==========================================================================
    
    @objc private func makeTransferButtonPressed(button: UIButton) {
        guard let _ = self.fromAccount, let _ = self.toAccount, let text = self.transferAmountTextField.text else {
            return
        }
        self.loadingScreen.show(inView: self.view)
        
        let amount : Double = Double(text) ?? 0
        
        self.transferViewWorker.makeTransfer(fromAccount: self.fromAccount!, toAccount: self.toAccount!, amount: amount)
    }
}

//==========================================================================
//MARK:- Extension: UITextFieldDelegate
//==========================================================================

extension TransferViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.isEmpty {
            return true
        }
        
        guard CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: string)) else {
            return false
        }
        
        guard let text = textField.text else { return true }
        
        let count = text.count + string.count - range.length
        return count <= Constants.TextFieldCharacterLimit
    }
}

//==========================================================================
//MARK:- Extension: TransferViewWorkerProtocol
//==========================================================================

extension TransferViewController: TransferViewWorkerProtocol {
    func transferCompleted(account: Account) {
        self.loadingScreen.hide()
        self.fromAccount = account
        self.finishSetup()
        
        AlertManager.showAlert(title: "Done!", message: "Transfer completed successfully.", inView: self.view)
    }
    
    func showError(error: MTError) {
        self.loadingScreen.hide()
        AlertManager.showAlert(error: error, inView: self.view)
    }
}
