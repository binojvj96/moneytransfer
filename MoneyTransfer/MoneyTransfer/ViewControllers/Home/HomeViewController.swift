//
//  HomeViewController.swift
//  MoneyTransfer
//
//  Created by Binoj V Janardhanan on 14/2/2019.
//  Copyright © 2019 MyBank. All rights reserved.
//

import UIKit
import PureLayout
import UIDeviceComplete
import PromiseKit

class HomeViewController: UIViewController {
    
    //==========================================================================
    //MARK:- UI Variables
    //==========================================================================
    
    private var mainContainerView: UIView = UIView()
    
    private var accountBalanceCardContainerView1: UIView = UIView()
    private var accountBalanceCardContainerView2: UIView = UIView()
    
    private var accountBalanceView1: AccountCardView! {
        didSet { self.accountBalanceView1.delegate = self }
    }
    private var accountBalanceView2: AccountCardView! {
        didSet { self.accountBalanceView2.delegate = self }
    }
    
    private var loadingScreen: LoadingScreen = LoadingScreen()
    
    //==========================================================================
    //MARK:- Variables
    //==========================================================================
    
    private var homeViewWorker: HomeViewWorker!
    
    private var UISetupFinished: Bool = false
    
    //==========================================================================
    //MARK:- Constants
    //==========================================================================
    
    public struct Constants {
        static let CommonMarginFromEdges: CGFloat = UIDevice.current.dc.isIpad ? 30.0 : 20.0
        static let MainContainerLeftRightMargin: CGFloat = UIDevice.current.dc.isIpad ? UIScreen.main.bounds.size.width/3 : CommonMarginFromEdges
        static let BalanceContainerHeight: CGFloat = 150.0
        static let TransferButtonHeight: CGFloat = 50.0
    }
    
    //==========================================================================
    //MARK:- Lifecycle Methods
    //==========================================================================
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        self.setupController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.applyViewStyle()
        self.finishControllerSetup()
        self.view.setNeedsLayout()
        
        self.triggerAccountDataFetch()
    }
    
    //==========================================================================
    //MARK:- Initial Setup methods
    //==========================================================================
    
    private func setupController() {
        self.homeViewWorker = HomeViewWorker()
        self.homeViewWorker.delegate = self
    }
    
    private func setupView() {
        
        if self.UISetupFinished { return }
        
        self.view.addSubview(self.mainContainerView)
        self.mainContainerView.autoPinEdgesToSuperviewSafeArea()
        
        self.mainContainerView.addSubview(self.accountBalanceCardContainerView1)
        self.accountBalanceCardContainerView1.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: Constants.CommonMarginFromEdges, left: Constants.MainContainerLeftRightMargin, bottom: 0.0, right: Constants.MainContainerLeftRightMargin), excludingEdge: .bottom)
        self.accountBalanceCardContainerView1.autoSetDimension(.height, toSize: Constants.BalanceContainerHeight)
        
        self.mainContainerView.addSubview(self.accountBalanceCardContainerView2)
        self.accountBalanceCardContainerView2.autoPinEdge(.leading, to: .leading, of: self.accountBalanceCardContainerView1)
        self.accountBalanceCardContainerView2.autoPinEdge(.trailing, to: .trailing, of: self.accountBalanceCardContainerView1)
        self.accountBalanceCardContainerView2.autoPinEdge(.top, to: .bottom, of: self.accountBalanceCardContainerView1, withOffset: Constants.CommonMarginFromEdges)
        self.accountBalanceCardContainerView2.autoSetDimension(.height, toSize: Constants.BalanceContainerHeight)
    }
    
    private func applyViewStyle() {
        
        if self.UISetupFinished { return }
        
        ThemeManager.sharedInstance.homeVCViewStyle(view: self.view)
        ThemeManager.sharedInstance.homeVCMainContainerViewStyle(view: self.mainContainerView)
        ThemeManager.sharedInstance.homeVCAccountBalanceCardViewStyle(view: self.accountBalanceCardContainerView1)
        ThemeManager.sharedInstance.homeVCAccountBalanceCardViewStyle(view: self.accountBalanceCardContainerView2)
    }
    
    private func finishControllerSetup() {
        
        if self.UISetupFinished { return }
        
        self.accountBalanceView1 = AccountCardView.init(frame: CGRect(origin: CGPoint.zero, size: self.accountBalanceCardContainerView1.frame.size))
        self.accountBalanceView2 = AccountCardView.init(frame: CGRect(origin: CGPoint.zero, size: self.accountBalanceCardContainerView2.frame.size))
        
        self.accountBalanceCardContainerView1.addSubview(self.accountBalanceView1)
        self.accountBalanceView1.autoPinEdgesToSuperviewEdges()
        
        self.accountBalanceCardContainerView2.addSubview(self.accountBalanceView2)
        self.accountBalanceView2.autoPinEdgesToSuperviewEdges()
        
        self.UISetupFinished = true
    }
    
    //==========================================================================
    //MARK:- Private Methods Methods
    //==========================================================================
    
    func triggerAccountDataFetch() {
        self.loadingScreen.show(inView: self.mainContainerView)
        self.homeViewWorker.fetchAccountData()
    }
}

//==========================================================================
//MARK:- Extension: HomeViewWorkerProtocol
//==========================================================================

extension HomeViewController: HomeViewWorkerProtocol {
    func accountDataFetched(account1: Account, account2: Account) {
        self.loadingScreen.hide()
        self.accountBalanceView1.updateAccountBalance(account: account1)
        self.accountBalanceView2.updateAccountBalance(account: account2)
    }
    
    func showError(error: MTError) {
        AlertManager.showAlert(error: error, action: self.triggerAccountDataFetch, cancelAction: nil, inView: self.view)
    }
}

//==========================================================================
//MARK:- Extension: AccountCardViewDelegate
//==========================================================================

extension HomeViewController: AccountCardViewDelegate {
    func accountCardViewTapped(view: AccountCardView) {
        guard let _ = self.accountBalanceView1?.account, let _ = self.accountBalanceView2?.account else { return }
        
        let transferVC = TransferViewController(fromAccount: self.accountBalanceView1 == view ? self.accountBalanceView1!.account! : self.accountBalanceView2!.account!, toAccount: self.accountBalanceView1 == view ? self.accountBalanceView2!.account! : self.accountBalanceView1!.account!)
        
        self.navigationController?.pushViewController(transferVC, animated: true)
    }
}
