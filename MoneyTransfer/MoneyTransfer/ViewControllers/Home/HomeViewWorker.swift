//
//  HomeViewModel.swift
//  MoneyTransfer
//
//  Created by Binoj V Janardhanan on 18/2/2019.
//  Copyright © 2019 MyBank. All rights reserved.
//

import PromiseKit

protocol HomeViewWorkerProtocol {
    func accountDataFetched(account1: Account, account2: Account)
    func showError(error: MTError)
}

class HomeViewWorker {
    
    //==========================================================================
    //MARK:- Variables
    //==========================================================================
    
    var delegate: HomeViewWorkerProtocol?
    
    //==========================================================================
    //MARK:- Public Methods
    //==========================================================================
    
    func fetchAccountData() {
        
        let promise1 = ApiManager.sharedInstance.getAccount(accountId: 1)
        let promise2 = ApiManager.sharedInstance.getAccount(accountId: 2)
        
        firstly {
            when(fulfilled: promise1, promise2)
            }.done { account1, account2 in
                self.delegate?.accountDataFetched(account1: account1, account2: account2)
            }.ensure {
                //Nothing to do
            }.catch{ error in
                if let error = error as? MTError {
                    self.delegate?.showError(error: error)
                }
        }
    }
}

