//
//  Account.swift
//  MoneyTransfer
//
//  Created by Binoj V Janardhanan on 18/2/2019.
//  Copyright © 2019 MyBank. All rights reserved.
//

import ObjectMapper

class Account: Mappable {
    //==========================================================================
    //MARK:- Constants
    //==========================================================================
    
    struct Parsekeys {
        static let AccountId: String = "account.id"
        static let Balance: String = "account.balance"
        static let Currency: String = "account.currency"
    }
    
    //==========================================================================
    //MARK:- Variables
    //==========================================================================
    
    var accountId: Int
    var balance: Double
    var currency: String
    
    //==========================================================================
    //MARK:- init methods
    //==========================================================================
    
    required init?(map: Map) {
        self.accountId = 0
        self.balance = 0
        self.currency = ""
    }
    
    func mapping(map: Map) {
        self.accountId <- map[Parsekeys.AccountId]
        self.balance <- map[Parsekeys.Balance]
        self.currency <- map[Parsekeys.Currency]
    }
}
