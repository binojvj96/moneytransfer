//
//  MainNavigationController.swift
//  MoneyTransfer
//
//  Created by Binoj V Janardhanan on 14/2/2019.
//  Copyright © 2019 MyBank. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController {
    
    //==========================================================================
    //MARK:- Lifecycle Methods
    //==========================================================================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.delegate = self
    }
}

//===========================================================
//MARK:- Extension: UINavigationControllerDelegate
//===========================================================

extension MainNavigationController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        
        self.setPageTitle(viewController: viewController)
    }
    
    //MARK: Helper Methods
    
    private func setPageTitle(viewController: UIViewController) {
        if viewController.isKind(of: HomeViewController.self) {
            viewController.navigationItem.title = "Home"
        }
        else if viewController.isKind(of: TransferViewController.self) {
            viewController.navigationItem.title = "Transfer"
        }
    }
}
