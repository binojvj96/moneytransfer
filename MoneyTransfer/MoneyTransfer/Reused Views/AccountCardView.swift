//
//  AccountCardView.swift
//  MoneyTransfer
//
//  Created by Binoj V Janardhanan on 18/2/2019.
//  Copyright © 2019 MyBank. All rights reserved.
//

import UIKit

protocol AccountCardViewDelegate {
    func accountCardViewTapped(view: AccountCardView)
}

class AccountCardView: UIView {
    
    //==========================================================================
    //MARK:- UI Variables
    //==========================================================================
    
    private var accountBalanceTitleLabel: UILabel = UILabel()
    private var accountBalanceSeparatorView: UIView = UIView()
    private var accountBalanceLabelContainerView: UIView = UIView()
    private var accountBalanceLabel: UILabel = UILabel()
    
    //==========================================================================
    //MARK:- Variables
    //==========================================================================
    
    //Public
    var delegate: AccountCardViewDelegate?
    
    //Private Set, Public Read
    public private(set) var account: Account?
    
    //==========================================================================
    //MARK:- Constants
    //==========================================================================
    
    private struct Constants {
        static let CommonMarginFromEdges: CGFloat = HomeViewController.Constants.CommonMarginFromEdges
        static let SeparatorViewHeight: CGFloat = 2.0
    }
    
    //==========================================================================
    //MARK:- Init Methods
    //==========================================================================
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setupView()
        self.applyViewStyle()
        self.setNeedsLayout()
        
        self.setupTapGesture()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //==========================================================================
    //MARK:- Initial Setup methods
    //==========================================================================
    
    private func setupView() {
        self.backgroundColor = UIColor.clear
        
        self.addSubview(self.accountBalanceTitleLabel)
        self.accountBalanceTitleLabel.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: Constants.CommonMarginFromEdges, left: Constants.CommonMarginFromEdges, bottom: 0.0, right: Constants.CommonMarginFromEdges), excludingEdge: .bottom)
        
        self.addSubview(self.accountBalanceSeparatorView)
        self.accountBalanceSeparatorView.autoPinEdge(toSuperviewEdge: .leading, withInset: Constants.CommonMarginFromEdges)
        self.accountBalanceSeparatorView.autoPinEdge(toSuperviewEdge: .trailing, withInset: Constants.CommonMarginFromEdges)
        self.accountBalanceSeparatorView.autoPinEdge(.top, to: .bottom, of: self.accountBalanceTitleLabel, withOffset:Constants.CommonMarginFromEdges)
        self.accountBalanceSeparatorView.autoSetDimension(.height, toSize: Constants.SeparatorViewHeight)
        
        self.addSubview(self.accountBalanceLabelContainerView)
        self.accountBalanceLabelContainerView.backgroundColor = UIColor.clear
        self.accountBalanceLabelContainerView.autoPinEdges(toSuperviewMarginsExcludingEdge: .top)
        self.accountBalanceLabelContainerView.autoPinEdge(.top, to: .bottom, of: self.accountBalanceSeparatorView)
        
        self.accountBalanceLabelContainerView.addSubview(self.accountBalanceLabel)
        self.accountBalanceLabel.autoCenterInSuperview()
        self.accountBalanceLabel.autoPinEdge(toSuperviewEdge: .leading, withInset: Constants.CommonMarginFromEdges)
        self.accountBalanceLabel.autoPinEdge(toSuperviewEdge: .trailing, withInset: Constants.CommonMarginFromEdges)
    }
    
    private func applyViewStyle() {
        ThemeManager.sharedInstance.accountOverviewCardBalanceTitleLabelStyle(label: self.accountBalanceTitleLabel)
        ThemeManager.sharedInstance.accountOverviewCardBalanceLabelStyle(label: self.accountBalanceLabel)
        ThemeManager.sharedInstance.accountOverviewCardBalanceSeparatorViewStyle(view: self.accountBalanceSeparatorView)
    }
    
    private func setupTapGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.accountCardTouched))
        self.addGestureRecognizer(tap)
    }
    
    //==========================================================================
    //MARK:- Action methods
    //==========================================================================
    
    @objc private func accountCardTouched() {
        self.delegate?.accountCardViewTapped(view: self)
    }
    
    //==========================================================================
    //MARK:- Public methods
    //==========================================================================
    
    public func updateAccountBalance(account: Account) {
        self.account = account
        
        self.accountBalanceLabel.text = "\(self.account!.balance) \(self.account!.currency)"
        self.accountBalanceTitleLabel.text = "Account \(self.account!.accountId) Balance"
    }
}

//==========================================================================
//MARK:- Extension: Comparable
//==========================================================================

extension AccountCardView: Comparable {
    static func < (lhs: AccountCardView, rhs: AccountCardView) -> Bool {
        guard let _ = lhs.account?.accountId, let _ = rhs.account?.accountId else { return false }
        return lhs.account?.accountId == rhs.account?.accountId
    }
}
