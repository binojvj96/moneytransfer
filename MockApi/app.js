import express from 'express';
import db from './db/db';
import bodyParser from 'body-parser';


const app = express();
//Parse incoming requests data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/api/v1/accounts/:id', (req, res) => {
    const id = parseInt(req.params.id, 10);

    db.map((account) => {
        if (account.id === id) {
            return res.status(200).send({
                success: 'true',
                message: 'Account retrieved.',
                account: account
            });
        }
    });

    return res.status(404).send({
        success: 'false',
        message: 'Account does not exist',
    });
});

app.put('/api/v1/maketransfer', (req, res) => {

    if (!req.body.fromAccountId) {
        return res.status(400).send({
            success: 'false',
            message: 'Parameter Missing',
        });
    }

    const fromAccountId = parseInt(req.body.fromAccountId, 10);

    if (!req.body.toAccountId) {
        return res.status(400).send({
            success: 'false',
            message: 'Parameter Missing',
        });
    }

    const toAccountId = parseInt(req.body.toAccountId, 10);

    if (!req.body.transferAmount) {
        return res.status(400).send({
            success: 'false',
            message: 'Parameter Missing',
        });
    }

    const transferAmount = parseInt(req.body.transferAmount, 10);

    let fromAccountFound;
    let toAccountFound;
    let fromAccountIndex;
    let toAccountIndex;

    db.map((account, index) => {
        if (account.id === fromAccountId) {
            fromAccountFound = account;
            fromAccountIndex = index;
        }
    });

    if (!fromAccountFound) {
        return res.status(404).send({
            success: 'false',
            message: 'From Account does not exist',
          });
    }

    db.map((account, index) => {
        if (account.id === toAccountId) {
            toAccountFound = account;
            toAccountIndex = index;
        }
    });

    if (!toAccountFound) {
        return res.status(404).send({
            success: 'false',
            message: 'To Account does not exist',
          });
    }

    const fromAccountNewBalance = fromAccountFound.balance - transferAmount;
    const toAccountNewBalance = toAccountFound.balance + transferAmount;

    const updatedFromAccount = {
        id: fromAccountId,
        balance: fromAccountNewBalance,
        currency: fromAccountFound.currency
    };

    const updatedToAccount = {
        id: toAccountId,
        balance: toAccountNewBalance,
        currency: toAccountFound.currency
    };

    db.splice(fromAccountIndex, 1, updatedFromAccount);
    db.splice(toAccountIndex, 1, updatedToAccount);

    return res.status(201).send({
        success: 'true',
        message: 'Account transfer completed successfully',
        account: updatedFromAccount,
    });
})

const PORT = 5000

app.listen(PORT, () => {
    console.log(`Server Running at port ${PORT}`)
});